var fs = require("fs");
var multimeter = require('multimeter');
var multi = multimeter(process);
multi.charm.cursor(false);

String.prototype.pad = function(padString, padLength) {
  var str = this.toString();
  var lenPad = padLength - str.length;

  while(lenPad > 0) {
    str = padString+str;
    lenPad--;
  }

  return str;
}

function getInputFiles(inputFile) {
  var files = [];

  for(var n=1; n<=999; n++) {
    var filename = inputFile+"."+n.toString().pad("0", 3);
    if(!fs.existsSync(filename)) break;

    files.push(filename);
  }

  return files;
}

function getTotalSize(files) {
  var totalSize = 0;

  files.forEach(function(file) {
    totalSize += fs.statSync(file).size;
  });

  return totalSize;
}

var Joiner = function(inputFile, outputFile) {
  this.inputFile = inputFile;
  this.outputFile = outputFile;
  this.filesToJoin = getInputFiles(this.inputFile);
  this.joinedFiles = [];
  this.totalSize = getTotalSize(this.filesToJoin);
  var self = this;
};

Joiner.prototype.pipeFile = function pipeFile(callback) {
  if(this.filesToJoin.length === 0) {
    multi.charm.cursor(true);
    multi.write('\n').destroy();

    console.log('DONE! see '+this.outputFile);
    process.exit();
  }

  var file = this.filesToJoin.shift();
  var self = this;

  this.joinedFiles.push(file);
  var countJoined = this.joinedFiles.length;
  var totalFiles = countJoined + this.filesToJoin.length;
  var readStream = fs.createReadStream(file);
  var writeStream = fs.createWriteStream(self.outputFile, {
    flags: 'a',
    encoding: null
  });

  readStream.pipe(writeStream);

  var joinedSize = 0;
  var fileSize = fs.statSync(file).size;
  
  multi.write('['+countJoined+'/'+totalFiles+'] Merging '+file+'\n');
  multi.drop({
    width: 25,
    solid : {
      text : '|',
      foreground : 'white',
      background : 'blue'
    },
  }, function(bar) {
    readStream.on('data', function(chunk) {
      joinedSize += chunk.length;
      var percent = Math.round((joinedSize / fileSize)*100);
      bar.percent(percent);      
    });

    readStream.on('close', function() {
      multi.write('\n');
      self.pipeFile(callback);
    });
  });
}

Joiner.prototype.join = function() {
  if(this.filesToJoin.length == 0) {
    console.log("\n Whoops! there is no files named like '"+this.inputFile+".001', '"+this.inputFile+".002', etc\n");
    process.exit();
  }

  if(fs.existsSync(this.outputFile)) {
    fs.unlinkSync(this.outputFile);
  }

  console.log("# Joining "+this.filesToJoin.length+" files into "+this.outputFile+"\n");

  multi.on('^C', function() {
    multi.write('Canceling...');
    fs.unlink(self.outputFile, function(err) {
      if(err) throw err;
      
      multi.write(' OK!');
      multi.charm.cursor(true);
      multi.write('\n').destroy();
      process.exit();
    });
  });

  this.joinedSize = 0;
  return this.pipeFile();
};

module.exports = function(inputFile, outputFile) {
  return new Joiner(inputFile, outputFile);
};