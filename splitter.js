var fs = require("fs");
var multimeter = require('multimeter');
var multi = multimeter(process);
multi.charm.cursor(false);

String.prototype.pad = function(padString, padLength) {
  var str = this.toString();
  var lenPad = padLength - str.length;

  while(lenPad > 0) {
    str = padString+str;
    lenPad--;
  }

  return str;
}

function bytesToSize(intBytes, unit) {
  var B = 1;
  var K = 1000;
  var M = K*K;
  var G = M*K;

  if(!unit) {
    if(intBytes > G) {
      unit = 'G';
    } else if(intBytes > M) {
      unit = 'M';
    } else if(intBytes > K) {
      unit = 'K';
    } else {
      unit = 'B';
    }
  }

  var result = parseFloat(intBytes);

  switch(unit) {
    case 'G': result = parseFloat((intBytes/G).toFixed(2)); 
      break;
    case 'M': result = parseFloat((intBytes/M).toFixed(2));
      break;
    case 'K': result = parseFloat((intBytes/K).toFixed(2));
      break;
  }

  return result+unit;
}

function sizeToBytes(size) {
  var unit = size.charAt(size.length-1);
  var num = parseInt(size.match(/^\d+/).join(''));

  var B = 1;
  var K = 1000;
  var M = K*K;
  var G = M*K;

  var result = num;

  switch(unit) {
    case 'G': result = G*num;
      break;
    case 'M': result = M*num;
      break;
    case 'K': result = K*num;
      break;
  }

  return result;
}

var Splitter = function(inputFile, outputFile, chunk) {
  if(!fs.existsSync(inputFile)) {
    console.log("\n Whoops! Could not find input file "+inputFile+"\n");
    process.exit();
  }

  this.inputFile = inputFile;
  this.outputFile = outputFile;
  this.inputFileSize = fs.statSync(inputFile).size;

  if(/^\d+$/.test(chunk)) {
    this.countChunkFiles = parseInt(chunk);
    this.chunkSize = Math.ceil(this.inputFileSize/this.countChunkFiles);
  } else if(/^\d+(K|M|G)$/.test(chunk)) {
    this.chunkSize = sizeToBytes(chunk);
    this.countChunkFiles = Math.ceil(this.inputFileSize/this.chunkSize);
  } else {
    console.log("\n Whoops! chunk value not allowed, try these: 500K, 10M, 1G or 10");
    process.exit();
  }

};

function writeFile(filename, start, end, splitter) {
  var joinedSize = 0;
  var fileSize = end - start;
  var readStream = fs.createReadStream(splitter.inputFile, {start: start, end: end});
  var writeStream = fs.createWriteStream(filename, {flags:'w', encoding:null});

  readStream.pipe(writeStream);

  multi.write('\n'+filename+' ');
  multi.drop({
    width: 25,
    solid : {
      text : '|',
      foreground : 'white',
      background : 'blue'
    },
  }, function(bar) {
    readStream.on('data', function(chunk) {
      //fs.appendFile(filename, chunk);
      joinedSize += chunk.length;
      var percent = Math.round((joinedSize / fileSize)*100);
      bar.percent(percent);      
    });
    readStream.on('close', function() {
      splitter.checkEnd();
    });
  });
}

Splitter.prototype.checkEnd = function() {
  this.countEnd++;

  if(this.countEnd >= this.countChunkFiles) {
    multi.charm.cursor(true);
    multi.write('\n').destroy();

    console.log('\nDONE!');
    process.exit();
  }
}

Splitter.prototype.split = function() {
  this.countEnd = 0;
  var self = this;
  var s,e;

  console.log('Splitting '+this.inputFile+' into '+this.countChunkFiles+' files ('+bytesToSize(this.chunkSize)+')');

  for(i=1; i<=this.countChunkFiles; i++) {

    if(i == 1) {
      s = 0;
      e = s+this.chunkSize-1;
    } else if(i != this.countChunkFiles){
      s = e+1;
      e = s+this.chunkSize;
    } else {
      s = e+1;
      e = this.inputFileSize;
    }

    var filename = this.outputFile+"."+i.toString().pad('0',3);
    writeFile(filename, s, e, self);
  }

}

module.exports = function(inputFile, outputFile, chunk) {
  return new Splitter(inputFile, outputFile, chunk);
};